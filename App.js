import * as React from 'react';
import { Image, Platform, StyleSheet, Text, TextInput, TouchableOpacity, View, ToolbarAndroid, StatusBar, Button, ActivityIndicator } from 'react-native'
import { SplashScreen } from 'expo';
import * as Font from 'expo-font';
import { Ionicons } from '@expo/vector-icons';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import BottomTabNavigator from './navigation/BottomTabNavigator';
import useLinking from './navigation/useLinking';

const Stack = createStackNavigator();

// export default function App(props) {
//   const [isLoadingComplete, setLoadingComplete] = React.useState(false);
//   const [initialNavigationState, setInitialNavigationState] = React.useState();
//   const containerRef = React.useRef();
//   const { getInitialState } = useLinking(containerRef);

//   // Load any resources or data that we need prior to rendering the app
//   React.useEffect(() => {
//     async function loadResourcesAndDataAsync() {
//       try {
//         SplashScreen.preventAutoHide();

//         // Load our initial navigation state
//         setInitialNavigationState(await getInitialState());

//         // Load fonts
//         await Font.loadAsync({
//           ...Ionicons.font,
//           'space-mono': require('./assets/fonts/SpaceMono-Regular.ttf'),
//         });
//       } catch (e) {
//         // We might want to provide this error information to an error reporting service
//         console.warn(e);
//       } finally {
//         setLoadingComplete(true);
//         SplashScreen.hide();
//       }
//     }

//     loadResourcesAndDataAsync();
//   }, []);

//   if (!isLoadingComplete && !props.skipLoadingScreen) {
//     return null;
//   } else {
//     return (
//       <View style={styles.container}>
//         {Platform.OS === 'ios' && <StatusBar barStyle="default" />}
//         <NavigationContainer ref={containerRef} initialState={initialNavigationState}>
//           <Stack.Navigator>
//             <Stack.Screen name="Root" component={BottomTabNavigator} />
//           </Stack.Navigator>
//         </NavigationContainer>
//       </View>
//     );
//   }
// }

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     backgroundColor: '#fff',
//   },
// });


export default class App extends React.Component {

  constructor(props) {
    super(props)

    this.state = {
      name: "",
      phone: "",
      email: "",
      password: "",
      password_confirmation: "",
      isLoading: false
    }
  }

  doRegister = () => {
    console.log('run')
    this.setState({isLoading: true})

    fetch('https://recipe.timedoor.id/api/v1/register', {
      method: 'POST',
      crossDomain: true,
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        name: this.state.name,
        phone: this.state.phone,
        email: this.state.email,
        password: this.state.password,
        password_confirmation: this.state.password_confirmation
      })
    })
    .then((response) => response.json())
    .then((responseJson) => {
      this.setState({isLoading: false})
      if (responseJson.error) {
        alert(JSON.stringify(responseJson.error.title))
      } else {

      }
    })
    .catch((error) => {
      this.setState({isLoading: false})
      console.error(error)
    })
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={{marginTop: 20}}>Recipe</Text>
        <TextInput
          style={styles.textInput}
          placeholder="Name"
          value={this.state.name}
          onChangeText={(text) => this.setState({name: text})}
          />
        <TextInput
          style={styles.textInput}
          placeholder="Phone"
          value={this.state.phone}
          onChangeText={(text) => this.setState({phone: text})}
          />
        <TextInput
          style={styles.textInput}
          placeholder="Email"
          value={this.state.email}
          onChangeText={(text) => this.setState({email: text})}
          />
        <TextInput
          style={styles.textInput}
          placeholder="Password"
          value={this.state.password}
          onChangeText={(text) => this.setState({password: text})}
          />
        <TextInput
          style={styles.textInput}
          placeholder="Confirm Password"
          value={this.state.password_confirmation}
          onChangeText={(text) => this.setState({password_confirmation: text})}
          />
        <TouchableOpacity
          style={styles.button}
          onPress={this.doRegister}>
            { this.state.isLoading ? (
              <ActivityIndicator size="large" color="#0000ff" />
            ) : (
              <Text style={{color:'white'}} >Register</Text>
            )}
          </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    marginTop: (Platform.OS == 'ios') ? 18 : StatusBar.currentHeight
  },
  textInput: {
    alignSelf: 'center',
    borderWidth: 1,
    width: 200,
    paddingHorizontal: 10,
    paddingVertical: 5,
    marginTop: 16,
    borderRadius: 5
  },
  button: {
    marginTop: 20,
    width: 200,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'blue',
    borderRadius: 10
  }
});
